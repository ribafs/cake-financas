# Cake-Finanças

Usando o CakePHP 3

Pequeno aplicativo em CakePHP 3 com exemplo de lógica de negícios, com código customizado para comunicação entre view e controller e model.

## Codificação

O grande objetivo aqui é mostrar como crio um form (Despesas/index.ctp) que chama um controller/action (DespesasController/index()).

O controller chama uma função de um model e recebe seu retorno.

Finalmente o controller/action devolve o resultado para uma view/template.

Abordando aqui a lógica de negócios de um pequeno aplicativo.

## Pequena Documentação

Veja detalhes no arquivo Docs.pdf.

## Download
https://github.com/ribafs/cake-financas

## Instalação
Este é um pacote completo, mas para instalar:
- Baixe para seu documentroot num diretório (financas)
- Crie o banco (financas)
- Importe o script .sql que acompanha
- Configure o config/app.php

Chame com:
http://localhost/financas


Ribamar FS
